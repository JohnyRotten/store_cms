require 'faker'

User.delete_all
Page.delete_all
New.delete_all
Category.delete_all
Good.delete_all
Comment.delete_all
Question.delete_all
PropertyType.delete_all
Property.delete_all
PropertyValue.delete_all

admin = User.create! login: 'admin', email: 'admin@example.com', password: '123456', password_confirmation: '123456', admin: true, enabled: true
sid = User.create! login: 'sid', email: 'mr.alegon@yandex.ru', password: '0GTPjX', password_confirmation: '0GTPjX', admin: true, enabled: true

1.upto(50) do |x|
  begin
    login = "#{Faker:value.last_name} #{Faker:value.first_name}"[0..14]
    password = 'usertest'
    email = Faker::Internet.email
    User.create! login: login, password: password, password_confirmation: password, email: email
  rescue
  end
end

1.upto(20) do |x|
  begin
    admin.pages.create!(
        title: Faker::Lorem.word,
        description: Faker::Lorem.paragraph,
        body: Faker::Lorem.paragraphs(2).join('<br>'),
        meta_title: Faker::Lorem.word,
        meta_desc: Faker::Lorem.sentence(4),
        meta_keywords: Faker::Lorem.words(4).join(' '),
        enabled: true,
        commentabled: true,
        href: '/' + Faker::Lorem.word
    )
  rescue
  end
  begin
    sid.pages.create!(
        title: Faker::Lorem.word,
        description: Faker::Lorem.paragraph,
        body: Faker::Lorem.paragraphs(2).join('<br>'),
        meta_title: Faker::Lorem.word,
        meta_desc: Faker::Lorem.sentence(4),
        meta_keywords: Faker::Lorem.words(4).join(' '),
        enabled: true,
        href: '/' + Faker::Lorem.word
    )
  rescue
  end
end

admin.pages.create! title: 'Помощь', description: 'Справочная информация для посетителей сайта', body: '<h1>Помощь</h1>', enabled: true, href: '/help'

1.upto(40) do
  begin
    New.create! title: Faker::Lorem.sentence(4), description: Faker::Lorem.sentence(10), body: Faker::Lorem.paragraphs(3).join('<br>')
  rescue
  end
end

cats = []

1.upto(4).each do
  begin
    cats << Category.create!(name: Faker::Commerce.department[0..19], description: Faker::Lorem.paragraph, enabled: true)
  rescue
  end
end

cats.each do |cat|
  1.upto(3).each do
    begin
      cat.categories.create!(name: Faker::Commerce.department[0..19], description: Faker::Lorem.paragraph, enabled: true)
    rescue
    end
    begin
      cat.goods.create!(name: Faker::Commerce.product_name, description: Faker::Lorem.paragraph, price: Faker::Commerce.price * 99, art: Faker::Code.ean)
    rescue
    end
  end
  cat.categories.each do |x|
    1.upto(3).each do
      begin
        x.goods.create!(name: Faker::Commerce.product_name, description: Faker::Lorem.paragraph, price: Faker::Commerce.price * 97, art: Faker::Code.ean)
      rescue
      end
      begin
        x.categories.create!(name: Faker::Commerce.department[0..19], description: Faker::Lorem.paragraph, enabled: true)
      rescue
      end
      x.categories.each do |y|
        begin
          y.goods.create!(name: Faker::Commerce.product_name, description: Faker::Lorem.paragraph, price: Faker::Commerce.price * 91, art: Faker::Code.ean)
        rescue
        end
      end
    end
  end
end

1.upto(10).each do 
  begin
    Question.create! question: Faker::Lorem.paragraph, answer: Faker::Lorem.paragraph, enabled: true
  rescue
  end
end

pt1 = PropertyType.create! name: 'Integer', description: 'Integer', regexp: '/^(\d+)$/',                        view: '<%= f.number_field :value %>'
pt2 = PropertyType.create! name: 'String',  description: 'String',  regexp: '/^(\w+)$/',                        view: '<%= f.text_field :value %>'
pt3 = PropertyType.create! name: 'Boolean', description: 'Boolean', regexp: '/^([01])$/',                       view: '<%= f.check_box :value %>'
pt4 = PropertyType.create! name: 'Date',    description: 'Date',    regexp: '/^((\d{4})\-(\d{2})\-(\d{2}))$/',  view: '<%= f.date_field :value %>'

pt1.properties.create! name: 'Вес', description: 'Вес'
pt1.properties.create! name: 'Цена', description: 'Цена'
pt1.properties.create! name: 'Высота', description: 'Высота'

pt2.properties.create! name: 'Материал', description: 'Материал'

pt3.properties.create! name: 'В наличии', description: 'В наличии'

pt4.properties.create! name: 'Создано', description: 'Дата изготовления'