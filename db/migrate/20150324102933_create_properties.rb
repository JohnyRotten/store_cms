class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :name
      t.text :description
      t.integer :property_type_id
      t.timestamps null: false
    end
    add_index :properties, :name, unique: true
    add_index :properties, :property_type_id
  end
end
