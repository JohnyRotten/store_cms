class CreatePropertyTypes < ActiveRecord::Migration
  def change
    create_table :property_types do |t|
      t.string :name
      t.text :description
      t.string :regexp
      t.text :view
      t.timestamps null: false
    end
    add_index :property_types, :name, unique: true
  end
end
