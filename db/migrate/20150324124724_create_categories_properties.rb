class CreateCategoriesProperties < ActiveRecord::Migration
  def change
    create_table :categories_properties do |t|
      t.integer :property_id
      t.integer :category_id
      t.timestamps null: false
    end
    add_index :categories_properties, :property_id
    add_index :categories_properties, :category_id
  end
end
