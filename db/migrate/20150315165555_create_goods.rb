class CreateGoods < ActiveRecord::Migration
  def change
    create_table :goods do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.string :art
      t.integer :category_id, default: 0
      t.boolean :enabled, default: false
      t.integer :count, default: 0

      t.timestamps null: false
    end

    add_index :goods, :name, unique: true
    add_index :goods, :art, unique: true
    add_index :goods, :category_id
  end
end
