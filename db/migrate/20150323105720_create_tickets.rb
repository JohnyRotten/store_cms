class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.text :body
      t.string :title
      t.integer :status, default: 0
      t.integer :priority, default: 0

      t.timestamps null: false
    end
    add_index :tickets, :user_id
    add_index :tickets, [ :user_id, :title ], unique: true
  end
end
