class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :description
      t.text :body
      t.boolean :enabled, default: false

      t.timestamps null: false
    end

    add_index :news, :title, unique: true
  end
end
