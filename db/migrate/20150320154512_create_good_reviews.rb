class CreateGoodReviews < ActiveRecord::Migration
  def change
    create_table :good_reviews do |t|
      t.integer :good_id
      t.integer :user_id
      t.integer :mark
      t.string :title
      t.text :body

      t.timestamps null: false
    end

    add_index :good_reviews, [ :good_id, :user_id ], unique: true
    add_index :good_reviews, :user_id
    add_index :good_reviews, :good_id
  end
end
