class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :good_id
      t.decimal :price
      t.integer :count

      t.timestamps null: false
    end
    add_index :order_items, :order_id
    add_index :order_items, :good_id
  end
end
