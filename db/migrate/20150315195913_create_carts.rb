class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :user_id
      t.integer :good_id
      t.decimal :price

      t.timestamps null: false
    end

    add_index :carts, [ :user_id, :good_id ], unique: true
  end
end
