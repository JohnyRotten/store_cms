class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :email
      t.string :password_digest
      t.boolean :enabled, default: false
      t.boolean :admin, default: false
      t.string :remember_token

      t.timestamps null: false
    end

    add_index :users, :login, unique: true
    add_index :users, :email, unique: true
  end
end
