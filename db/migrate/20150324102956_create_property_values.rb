class CreatePropertyValues < ActiveRecord::Migration
  def change
    create_table :property_values do |t|
      t.integer :good_id
      t.integer :property_id
      t.string :value, default: ''
      t.timestamps null: false
    end
    add_index :property_values, :good_id
    add_index :property_values, :property_id
  end
end
