class AddColumnCommentEnabledToPages < ActiveRecord::Migration
  def change
  	add_column :pages, :commentabled, :boolean, default: false
  end
end
