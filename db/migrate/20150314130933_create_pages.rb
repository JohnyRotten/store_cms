class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :description
      t.text :body
      t.integer :user_id
      t.string :meta_title
      t.string :meta_desc
      t.string :meta_keywords
      t.boolean :enabled, default: false

      t.timestamps null: false
    end

    add_index :pages, :title, unique: true
    add_index :pages, :user_id
  end
end
