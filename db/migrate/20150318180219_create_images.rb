class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :good_id
      t.string :title, default: ''
      t.string :alt, default: ''
      t.attachment :path

      t.timestamps null: false
    end
    add_index :images, :good_id
  end
end
