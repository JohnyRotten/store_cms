class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :good_id
      t.integer :user_id
      t.timestamps null: false
    end

    add_index :bookmarks, [ :good_id, :user_id ], unique: true
  end
end
