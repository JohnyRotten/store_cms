class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :user_id
      t.integer :good_id

      t.timestamps null: false
    end

    add_index :likes, [ :user_id, :good_id ], unique: true
  end
end
