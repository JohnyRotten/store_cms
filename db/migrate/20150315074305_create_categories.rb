class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.boolean :enabled, default: false
      t.integer :category_id, default: 0

      t.timestamps null: false
    end

    add_index :categories, :name, unique: true
    add_index :categories, :category_id
  end
end
