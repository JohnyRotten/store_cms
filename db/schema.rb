# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150324124724) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "good_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bookmarks", ["good_id", "user_id"], name: "index_bookmarks_on_good_id_and_user_id", unique: true, using: :btree

  create_table "carts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "good_id"
    t.decimal  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "carts", ["user_id", "good_id"], name: "index_carts_on_user_id_and_good_id", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "enabled",     default: false
    t.integer  "category_id", default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "categories", ["category_id"], name: "index_categories_on_category_id", using: :btree
  add_index "categories", ["name"], name: "index_categories_on_name", unique: true, using: :btree

  create_table "categories_properties", force: :cascade do |t|
    t.integer  "property_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "categories_properties", ["category_id"], name: "index_categories_properties_on_category_id", using: :btree
  add_index "categories_properties", ["property_id"], name: "index_categories_properties_on_property_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "message"
    t.boolean  "visibled",         default: true
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "good_reviews", force: :cascade do |t|
    t.integer  "good_id"
    t.integer  "user_id"
    t.integer  "mark"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "good_reviews", ["good_id", "user_id"], name: "index_good_reviews_on_good_id_and_user_id", unique: true, using: :btree
  add_index "good_reviews", ["good_id"], name: "index_good_reviews_on_good_id", using: :btree
  add_index "good_reviews", ["user_id"], name: "index_good_reviews_on_user_id", using: :btree

  create_table "goods", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price"
    t.string   "art"
    t.integer  "category_id", default: 0
    t.boolean  "enabled",     default: false
    t.integer  "count",       default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "goods", ["art"], name: "index_goods_on_art", unique: true, using: :btree
  add_index "goods", ["category_id"], name: "index_goods_on_category_id", using: :btree
  add_index "goods", ["name"], name: "index_goods_on_name", unique: true, using: :btree

  create_table "images", force: :cascade do |t|
    t.integer  "good_id"
    t.string   "title",             default: ""
    t.string   "alt",               default: ""
    t.string   "path_file_name"
    t.string   "path_content_type"
    t.integer  "path_file_size"
    t.datetime "path_updated_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "images", ["good_id"], name: "index_images_on_good_id", using: :btree

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "good_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "likes", ["user_id", "good_id"], name: "index_likes_on_user_id_and_good_id", unique: true, using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.text     "body"
    t.boolean  "enabled",     default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "news", ["title"], name: "index_news_on_title", unique: true, using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "good_id"
    t.decimal  "price"
    t.integer  "count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "order_items", ["good_id"], name: "index_order_items_on_good_id", using: :btree
  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "cost"
    t.boolean  "done",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "pages", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.text     "body"
    t.integer  "user_id"
    t.string   "meta_title"
    t.string   "meta_desc"
    t.string   "meta_keywords"
    t.boolean  "enabled",       default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "href"
    t.boolean  "commentabled",  default: false
  end

  add_index "pages", ["title"], name: "index_pages_on_title", unique: true, using: :btree
  add_index "pages", ["user_id"], name: "index_pages_on_user_id", using: :btree

  create_table "properties", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "property_type_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "properties", ["name"], name: "index_properties_on_name", unique: true, using: :btree
  add_index "properties", ["property_type_id"], name: "index_properties_on_property_type_id", using: :btree

  create_table "property_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "regexp"
    t.text     "view"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "property_types", ["name"], name: "index_property_types_on_name", unique: true, using: :btree

  create_table "property_values", force: :cascade do |t|
    t.integer  "good_id"
    t.integer  "property_id"
    t.string   "value",       default: ""
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "property_values", ["good_id"], name: "index_property_values_on_good_id", using: :btree
  add_index "property_values", ["property_id"], name: "index_property_values_on_property_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "question"
    t.string   "answer",     default: ""
    t.boolean  "enabled",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "questions", ["question"], name: "index_questions_on_question", unique: true, using: :btree

  create_table "tickets", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "body"
    t.string   "title"
    t.integer  "status",     default: 0
    t.integer  "priority",   default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "tickets", ["user_id", "title"], name: "index_tickets_on_user_id_and_title", unique: true, using: :btree
  add_index "tickets", ["user_id"], name: "index_tickets_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "email"
    t.string   "password_digest"
    t.boolean  "enabled",         default: false
    t.boolean  "admin",           default: false
    t.string   "remember_token"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["login"], name: "index_users_on_login", unique: true, using: :btree

end
