require 'rails_helper'

describe User do
  before { @user = User.new(login: 'example', email: 'example@example.com',
                            password: 'example', password_confirmation: 'example') }
  subject { @user }

  it { should respond_to(:login) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:enabled) }
  it { should respond_to(:admin) }
  it { should respond_to(:remember_token) }

  it { should be_valid }
  it { should_not be_admin }
  it { should_not be_enabled }

  describe 'Login' do

    describe 'short login' do
      before { @user.login = '12' }
      it { should_not be_valid }
    end

    describe 'empty login' do
      before { @user.login = '' }
      it { should_not be_valid }
    end
  end

  describe 'Email' do
    describe 'empty email' do
      before { @user.email = '' }
      it { should_not be_valid }
    end

    it 'should be invalid' do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar+baz.com foo@bar..com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
      end
    end

    it 'should be valid' do
      addresses = %w[user@foo.com user@foo.org example.user@foo.com
			               foo@bar_baz.com foo@bar.com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).to be_valid
      end
    end
  end
end
