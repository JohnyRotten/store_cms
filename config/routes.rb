Rails.application.routes.draw do
  get 'bookmarks/index'

  mount Ckeditor::Engine => '/ckeditor'
  root 'goods#index'
  resources :users, :pages, :news, :goods
  resources :sessions, only: [ :new, :create, :destroy ]
  resources :categories, only: [ :index, :show, :create, :update, :destroy ]
  resources :carts, only: [ :index, :create, :destroy ]
  resources :orders, only: [ :index, :create, :destroy ]
  resources :images, only: [ :create, :destroy ]
  resources :bookmarks, only: [ :index, :create, :destroy ]
  resources :good_reviews, only: [ :create, :update, :destroy ]
  resources :likes, only: [ :create, :destroy ]
  
  resources :questions, only: [ :index, :create, :update, :destroy ]
  resources :tickets, only: [ :index, :show, :new, :create, :update, :destroy ]

  # Comments
  resources :comments, only: [ :create, :destroy ]

  # Properties
  resources :property_types, only: [ :index, :create, :update, :destroy ]
  resources :properties, only: [ :index, :create, :update, :destroy ]
  resources :property_values, only: [ :create, :update ]
  match '/categories/:cid/properties/:pid', to: 'categories#include', via: 'post'
  match '/categories/:cid/properties/:pid', to: 'categories#exclude', via: 'delete'

  match '/signup',  to: 'users#new',        via: 'get'
  match '/signin',  to: 'sessions#new',     via: 'get'
  match '/signout', to: 'sessions#destroy', via: 'delete'

  match '/search',  to: 'static_pages#search', via: 'get'
  match '/admin',   to: 'static_pages#admin',  via: 'get'

  get '/:href',     to: 'pages#open_url'
end
