module ApplicationHelper

  def full_title(title)
    base_title = 'Store CMS'
    return "#{base_title} | #{title}" unless title.empty?
    return base_title
  end

end
