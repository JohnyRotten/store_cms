class PropertyTypesController < ApplicationController
	before_action :admin_user

	def index
		@property_types = PropertyType.all
	end

	def create
		@type = PropertyType.create(type_params)
		@property_types = PropertyType.all
		respond_to do |format|
			if @type.save
				format.html { redirect_to property_types_path, success: 'Тип создан' }
				format.js {}
				format.json { render json: @type, status: :created, location: @type }
			else
				format.html { redirect_to property_types_path, danger: 'Ошибка при создании свойства' }
				formta.json { render json: @type.errors, status: :uprocessable_entity }
			end
		end
	end

	def update
		@type = PropertyType.find(params[:id])
		@property_types = PropertyType.all
		respond_to do |format|
			if @type.update_attributes type_params
				format.html { redirect_to property_types_path, success: 'Тип обновлен' }
				format.js {}
				format.json { render json: @type, status: :updated, location: @type }
			else
				format.html { redirect_to property_types_path, danger: 'Ошибка при обновлении типа' }
				formta.json { render json: @type.errors, status: :uprocessable_entity }
			end
		end
	end

	def destroy
		PropertyType.find(params[:id]).destroy
		@property_types = PropertyType.all
		respond_to do |format|
			format.html { redirect_to property_types_path, success: 'Тип удален' }
			format.js {}
		end
	end

	private

		def type_params
			params.require(:property_type).permit(:name, :description, :regexp, :view)
		end
end
