class GoodReviewsController < ApplicationController
  before_action :signed_in_user, only: [ :create, :update, :destroy ]
  before_action :admin_user, only: [ :update, :destroy ]

  def create
    @review = current_user.good_reviews.create(good_review_params)
    if @review.save
      flash[:success] = 'Ваш отзыв добавлен'
    else
      flash[:danger] = 'Ошибка при добавлении отзыва'
      flash[:info] = @review.errors.inspect
    end
    redirect_to :back
  end

  def update
    @review = GoodReview.find(params[:id])
    if @review.update_attributes good_review_params
      flash[:success] = 'Отзыв изменен'
    else
      flash[:danger] = 'Ошибка изменения отзыва'
    end
    redirect_to @review.good
  end

  def destroy
    GoodReview.find(params[:id]).destroy
    flash[:success] = 'Отзыв удален'
    redirect_to :back
  end

  private

    def good_review_params
      params.require(:good_review).permit(:good_id, :title, :body, :mark)
    end
end
