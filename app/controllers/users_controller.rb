class UsersController < ApplicationController
  before_action :admin_user, only: [ :index, :destroy ]
  before_action :correct_or_admin_user, only: [ :show, :edit, :update ]
  before_action :auth_user, only: [ :new, :create ]

  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.save
      flash[:success] = 'Пользователь создан'
      redirect_to @user
    else
      flash[:error] = 'Ошибка создания пользователя'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes user_params
      flash[:success] = 'Данные пользователя обновлены'
      redirect_to @user
    else
      flash[:error] = 'Ошибка обновления данных пользователя'
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'Пользователь удален'
    redirect_to users_path
  end

  private

    def user_params
      params.require(:user).permit(:login, :email, :password, :password_confirmation)
    end
end
