class OrdersController < ApplicationController
  before_action :signed_in_user

  def index
    @orders = current_user.orders.order(:done, :updated_at).reverse_order
  end

  def create
    @order = current_user.orders.create
    if @order.save
      begin
        params[:order_items].each do |key, values|
          if values[:enabled] == 'on'
            @order.order_items.create!(price: current_user.carts.find_by(good_id: key.to_i).price, good_id: key.to_i, count: values[:count].to_i)
          end
        end
        flash[:success] = 'Заказ выполнен'
        redirect_to orders_path
      # rescue => e
      #   @order.destroy
      #   flash[:danger] = "Ошибка при добавлении позиции товара: \n#{e.message}\n}"
      #   redirect_to carts_path
      end
    else
      flash[:danger] = 'Ошибка'
      redirect_to carts_path
    end
  end

  def destroy
    current_user.orders.find(params[:id]).destroy
    flash[:success] = 'Заказ удален'
    redirect_to orders_path
  end

  private


end
