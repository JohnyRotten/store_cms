class SessionsController < ApplicationController
  before_action :auth_user, only: :new

  def create
    user = User.find_by(email: params[:session][:email])
    if user.authenticate params[:session][:password]
      sign_in user
      redirect_back_or current_user
    else
      flash.now[:error] = 'Неправильный email или пароль'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
end
