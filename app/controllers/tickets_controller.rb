class TicketsController < ApplicationController
	before_action :signed_in_user
	before_action :admin_or_owner, only: [ :show, :update, :destroy ]

	def index
		@tickets = current_user.tickets.paginate(page: params[:page])
	end

	def show
	end

	def new
		@ticket = Ticket.new
	end

	def create
		@ticket = current_user.tickets.create(ticket_params)
		if @ticket.save
			flash[:success] = 'Тикет создан'
			redirect_to @ticket
		else
			flash[:danger] = 'Ошибка при создании тикета'
			render 'new'
		end
	end

	def destroy
		@ticket.destroy
		flash[:success] = 'Тикет удален'
		redirect_to tickets_path
	end

	def update
		if @ticket.update_attribute :status, params[:ticket][:status]
			flash[:success] = 'Статус обновлен'
		else
			flash[:danger] = 'Ошибка обновления статуса'
		end
		redirect_to @ticket
	end

	private

		def admin_or_owner
			@ticket = Ticket.find(params[:id])
			redirect_to :back unless (admin? || current_user.tickets.include?(@ticket))
		end

		def ticket_params
			params.require(:ticket).permit(:title, :body, :prioritet, :status)
		end
end
