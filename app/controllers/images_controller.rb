class ImagesController < ApplicationController
  before_action :admin_user

  def create
    @image = Image.create(image_params)
    if @image.save
      flash[:success] = 'Изображение добавлено'
    else
      flash[:danger] = 'Ошибка при добавлении изображения'
      flash[:info] = @image.errors.messages.inspect
    end
    redirect_to edit_good_path(@image.good)
  end

  def destroy
    image = Image.find(params[:id])
    good = image.good
    image.path = nil
    image.save!
    image.destroy
    flash[:success] = 'Изображение удалено'
    redirect_to edit_good_path(good)
  end

  private

    def image_params
      params.require(:image).permit(:path, :alt, :title, :good_id)
    end
end
