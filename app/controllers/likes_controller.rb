class LikesController < ApplicationController
	before_action :signed_in_user

	def create
		@like = current_user.likes.create(like_params)
		if @like.save
			flash[:success] = 'Ваше мнение учтено'
		else
			flash[:danger] = 'Ошибка при учтении Вашего мнения'
		end
		redirect_to @like.good
	end

	def destroy
		like = Like.find(params[:id])
		good = like.good
		like.destroy
		flash[:success] = 'Ваше мнение учтено'
		redirect_to good
	end

	private

		def like_params
			params.require(:like).permit(:good_id)
		end
end
