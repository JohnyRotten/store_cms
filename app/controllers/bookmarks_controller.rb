class BookmarksController < ApplicationController
  before_action :signed_in_user

  def index
    @bookmarks = current_user.bookmarks.paginate(page: params[:page])
  end

  def create
    @bookmark = current_user.bookmarks.create(good_id: params[:bookmark][:good_id])
    if @bookmark.save
      flash[:success] = 'Товар добавлен в закладки'
    else
      flash[:danger] = 'Ошибка при добавлении товара'
    end
    redirect_to :back
  end

  def destroy
    current_user.bookmarks.find(params[:id]).destroy
    flash[:success] = 'Товар удален из закладок'
    redirect_to :back
  end
end
