class CartsController < ApplicationController
  before_action :signed_in_user

  def index
    @carts = current_user.carts
  end

  def create
    @cart = current_user.carts.create(cart_params)
    if @cart.save
      flash[:success] = 'Товар добавлен в корзину'
    else
      flash[:danger] = 'Ошибка при добавлении товара'
    end
    redirect_to :back
  end

  def destroy
    current_user.carts.find(params[:id]).destroy
    flash[:success] = 'Товар удален из корзины'
    redirect_to :back
  end

  private

    def cart_params
      params.require(:cart).permit(:good_id)
    end

end
