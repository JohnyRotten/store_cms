class PropertiesController < ApplicationController
	before_action :admin_user

	def index
		@properties = Property.all
	end

	def create
		@property = Property.create(property_params)
		@properties = Property.all
		respond_to do |format|
			if @property.save
				format.html { redirect_to properties_path, success: 'Свойство создано' }
				format.js {}
				format.json { render json: @property, status: :created, location: @property }
			else
				format.html { redirect_to properties_path, danger: 'Свойство не создано' }
				format.js {}
				format.json { render json: @property.errors, status: :uprocessable_entity }
			end
		end
	end

	def update
		@property = Property.find(params[:id])
		@properties = Property.all
		respond_to do |format|
			if @property.update_attributes property_params
				format.html { redirect_to properties_path, success: 'Свойство обновлено' }
				format.js {}
				format.json { render json: @property, status: :created, location: @property }
			else
				format.html { redirect_to properties_path, danger: 'Свойство не обновлено' }
				format.js {}
				format.json { render json: @property.errors, status: :uprocessable_entity }
			end
		end
	end

	def destroy
		Property.find(params[:id]).destroy
		@properties = Property.all
		respond_to do |format|
			format.html { redirect_to properties_path, success: 'Свойство удалено' }
			format.js {}
		end
	end

	private

		def property_params
			params.require(:property).permit(:name, :description, :property_type_id)
		end 
end
