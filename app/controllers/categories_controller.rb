class CategoriesController < ApplicationController
  before_action :signed_in_user
  before_action :admin_user
  before_action :get_categories_tree

  def index
  end

  def show
    @category = Category.find(params[:id])
    values = @category.all_goods
    @goods = WillPaginate::Collection.create(params[:page] || 1, 12, values.length) do |pager|
      pager.replace values
    end
    render 'goods/index'
  end

  def create
    @category = Category.create(category_params)
    if @category.save
      flash[:success] = 'Категория создана'
    else
      flash[:danger] = 'Ошибка при создании категории'
    end
    redirect_to categories_path
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes category_params
      flash[:success] = 'Категория обновлена'
    else
      flash[:danger] = 'Ошибка при обновлении категории'
      flash[:info] = @category.errors.inspect
    end
    redirect_to categories_path
  end

  def destroy
    Category.find(params[:id]).destroy
    flash[:success] = 'Категория удалена'
    redirect_to categories_path
  end

  def include
    property = Property.find(params[:pid])
    category = Category.find(params[:cid])
    category.properties << property
    if category.save
      flash[:success] = 'Свойство добавлено'
    else
      flash[:danger] = 'Ошибка при добавлении свойства'
    end
    redirect_to categories_path
  end

  def exclude
    Category.find(params[:cid]).properties.find(params[:pid]).destroy
    flash[:success] = 'Свойство удалено'
    redirect_to categories_path
  end

  private

    def category_params
      params.require(:category).permit(:name, :description, :enabled, :category_id)
    end

    def get_categories_tree
      @categories = Category.where(category_id: 0)
    end
end
