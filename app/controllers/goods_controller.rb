class GoodsController < ApplicationController
  before_action :signed_in_user, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :admin_user, only: [ :new, :create, :edit, :update, :destroy ]

  def index
    @goods ||= Good.paginate(page: params[:page], per_page: 12)
  end

  def show
    @good = Good.find(params[:id])
  end

  def new
    @good = Good.new
  end

  def create
    @good = Good.create good_params
    if @good.save
      flash[:success] = 'Товар успешно создан'
      redirect_to @good
    else
      flash[:danger] = 'Ошибка при создании товара'
      render 'new'
    end
  end

  def edit
    @good = Good.find(params[:id])
  end

  def update
    @good = Good.find(params[:id])
    if @good.update_attributes good_params
      flash[:success] = 'Товар обновлен'
      redirect_to @good
    else
      flash[:danger] = 'Ошибка обновления товара'
      render 'edit'
    end
  end

  def destroy
    Good.find(params[:id]).destroy
    flash[:success] = 'Товар удален'
    redirect_to goods_path
  end

  private

    def good_params
      params.require(:good).permit(:name, :description, :art, :price, :enabled, :count, :category_id)
    end

end
