class PropertyValuesController < ApplicationController
	before_action :admin_user

	def create
		@value = PropertyValue.create(value_params)
		if @value.save
			flash[:success] = 'Свойство создано'
		else
			flash[:danger] = 'Свойство не создано'
		end
		redirect_to :back
	end

	def update
		@value = PropertyValue.find(params[:id])
		if @value.update_attributes value_params
			flash[:success] = 'Свойство изменено'
		else
			flash[:danger] = 'Свойство не изменено'
		end
		redirect_to :back
	end

	private

		def value_params
			params.require(:property_value).permit(:good_id, :property_id, :value)
		end
end
