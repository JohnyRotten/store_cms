class CommentsController < ApplicationController
	before_action :signed_in_user
	before_action :admin_user, only: :destroy

	def create
		@comment = Comment.create(comment_params)
		if @comment.save
			flash[:success] = 'Комментарий добавлен'
		else
			flash[:danger] = 'Ошибка добавления комментария'
			flash[:info] = @comment.errors.inspect
		end
		redirect_to :back
	end

	def destroy
		Comment.find(params[:id]).destroy
		flash[:success] = 'Комментарий удален'
		redirect_to :back
	end

	private

		def comment_params
			h = params.require(:comment).permit(:commentable_type, :commentable_id, :message, :visibled).to_h
			h[:user_id] = current_user.id
			h
		end
end
