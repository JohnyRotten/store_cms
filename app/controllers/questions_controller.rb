class QuestionsController < ApplicationController
	before_action :admin_user, only: [ :update, :destroy ]

	def index
		@questions = Question.all
	end

	def create
		@question = Question.create(question_params)
		if @question.save
			flash[:success] = 'Ваш вопрос добавлен'
		else
			flash[:info] = @question.errors.inspect
			flash[:danger] = 'Ошибка при добавлении вопроса'
		end
		redirect_to questions_path
	end

	def update
		@question = Question.find(params[:id])
		if @question.update_attributes(question_params)
			flash[:success] = 'Вопрос отредактирован'
		else
			flash[:danger] = 'Ошибка при изменении вопроса'
		end
		redirect_to questions_path
	end

	def destroy
		Question.find(params[:id]).destroy
		flash[:success] = 'Вопрос удален'
		redirect_to questions_path
	end

	private

		def question_params
			params.require(:question).permit(:question, :answer, :enabled)
		end
end
