class PagesController < ApplicationController
  before_action :signed_in_user, only: [ :new, :create, :edit, :update ]
  before_action :owner_page, only: [ :edit, :update, :destroy ]

  def index
    @pages = Page.paginate(page: params[:page], per_page: 10)
  end

  def show
    @page = Page.find(params[:id])
  end

  def new
    @page = Page.new
  end

  def create
    @page = current_user.pages.create(page_params)
    if @page.save
      flash[:success] = 'Страница создана'
      if @page.enabled?
        redirect_to @page
      else
        redirect_to pages_path
      end
    else
      flash[:danger] = 'Ошибка при создании страницы'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @page.update_attributes page_params
      flash[:success] = 'Страница обновлена'
      if @page.enabled?
        redirect_to @page
      else
        redirect_to pages_path
      end
    else
      flash[:danger] = 'Ошибка при обновлении страницы'
      render 'new'
    end
  end

  def destroy
    @page.destroy
    flash[:success] = 'Страница удалена'
    redirect_to pages_path
  end

  def open_url
    @page = Page.find_by(href: '/' + params[:href])
    unless @page.nil?
      render 'show'
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  private

    def owner_page
      signed_in_user
      @page = Page.find(params[:id])
      unless current_user.pages.include? @page
        flash[:warning] = 'Ошибка доступа'
        redirect_to :back
      end
    end

    def page_params
      params.require(:page).permit(:title, :description, :body, :meta_title, :meta_desc, :meta_keywords, :enabled, :href, :commentabled)
    end
end
