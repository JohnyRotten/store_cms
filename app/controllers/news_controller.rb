class NewsController < ApplicationController
  before_action :admin_user, only: [ :new, :create, :edit, :update, :destroy ]

  def index
    @news = New.paginate(page: params[:page], per_page: 10)
  end

  def show
    @new = New.find(params[:id])
  end

  def new
    @new = New.new
  end

  def create
    @new = New.create(new_params)
    if @new.save
      flash[:success] = 'Новость добавлена'
      redirect_to news_path
    else
      flash[:danger] = 'Ошибка добавления новости'
      render 'new'
    end
  end

  def edit
    @new = New.find(params[:id])
  end

  def update
    @new = New.find(params[:id])
    if @new.update_attributes new_params
      flash[:success] = 'Новость обновлена'
      redirect_to @new
    else
      flash[:danger] = 'Ошибка изменения новости'
      render 'edit'
    end
  end

  def destroy
    New.find(params[:id]).destroy
    flash[:success] = 'Новость удалена'
    redirect_to news_path
  end

  private

    def new_params
      params.require(:new).permit(:title, :description, :body, :enabled)
    end
end
