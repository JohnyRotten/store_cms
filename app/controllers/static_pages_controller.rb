class StaticPagesController < ApplicationController
  before_action :admin_user, only: :admin

  def home
  end

  def search
  	q = params[:q]

  	@goods = Good.where('goods.name LIKE :l_q OR goods.description LIKE :l_q', { l_q: "%#{q}%" })
  	@news = New.where('news.title LIKE :l_q OR news.description LIKE :l_q', { l_q: "%#{q}%" })
  	@pages = Page.where('pages.title LIKE :l_q OR pages.description LIKE :l_q', { l_q: "%#{q}%" })
  end

  def admin
  	@goods 			= Good.all
  	@orders			= Order.all
  	@users 			= User.all
  	@news 			= New.all
  	@pages 			= Page.all
  	@tickets 		= Ticket.all
  	@categories 	= Category.all
  	@properties		= Property.all
  	@property_types = PropertyType.all
  end

end
