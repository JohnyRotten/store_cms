class PropertyType < ActiveRecord::Base
	has_many :properties, dependent: :destroy
	
	validates :name, presence: true, length: { in: 3..10 }
    validates :description, presence: true, length: { maximum: 75 }
    validates :regexp, presence: true
    validates :view, presence: true
end
