class PropertyValue < ActiveRecord::Base
	belongs_to :good
	belongs_to :property

	validates :good_id, presence: true
	validates :property_id, presence: true
end
