class New < ActiveRecord::Base
	has_many :comments, as: :commentable

	validates :title, presence: true, length: { in: 5..25 }, uniqueness: { case_sensitive: false }
	validates :description, presence: true
	validates :body, presence: true
end
