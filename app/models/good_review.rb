class GoodReview < ActiveRecord::Base
  belongs_to :good
  belongs_to :user

  has_many :comments, as: :commentable

  validates :title, presence: true, length: { in: 5..25 }
  validates :body, presence: true
  validates :mark, presence: true
  validates :user_id, presence: true
  validates :good_id, presence: true
end
