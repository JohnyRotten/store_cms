class User < ActiveRecord::Base
  has_secure_password

  has_many :pages
  has_many :carts, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :bookmarks, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :good_reviews, dependent: :destroy

  has_many :comments, dependent: :destroy
  has_many :comments, as: :commentable

  has_many :tickets, dependent: :destroy

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@\w+(\.[a-z]+)*\.[a-z]+\z/i

  validates :login, presence: true, length: { in: 3..15 }
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password,  presence: true, length: { minimum: 6 }
  validates :password_confirmation, presence: true

  before_save { email.downcase! }
  before_create :create_remember_token

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def bookmark?(good)
    bookmark(good).present?
  end

  def bookmark(good)
    self.bookmarks.find_by(good_id: good.id)
  end

  def like?(good)
    like(good).present?
  end

  def like(good)
    self.likes.find_by good_id: good.id
  end

  def review(good)
    self.good_reviews.find_by(good_id: good.id) || GoodReview.new
  end

  def open_tickets
    self.tickets.where.not(status: 3)
  end

  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
