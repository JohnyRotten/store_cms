class Comment < ActiveRecord::Base
	has_many :comments, as: :commentable
	belongs_to :commentable, polymorphic: true
	belongs_to :user

	validates :user_id, presence: true
	validates :commentable_id, presence: true
	validates :commentable_type, presence: true
	validates :message, presence: true
end
