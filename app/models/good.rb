class Good < ActiveRecord::Base
  belongs_to :category
  has_many :images, dependent: :destroy
  has_many :good_reviews, dependent: :destroy
  has_many :comments, as: :commentable
  has_many :property_values, dependent: :destroy

  has_many :carts, dependent: :destroy
  has_many :bookmarks, dependent: :destroy
  has_many :likes, dependent: :destroy

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :description, presence: true

  before_save { self.price = price.truncate(2) }

  def property_value(property)
    property_values.find_by(property_id: property.id) || self.property_values.create!(property_id: property.id)
  end


end
