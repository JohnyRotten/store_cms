class Property < ActiveRecord::Base
	has_many :property_value, dependent: :destroy
	belongs_to :property_type
	has_and_belongs_to_many :categories

	validates :name, presence: true, length: { in: 3..20 }
	validates :description, presence: true
end
