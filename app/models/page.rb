class Page < ActiveRecord::Base
  belongs_to :user
  has_many :comments, as: :commentable

  validates :title, presence: true, length: { in: 5..75 }
  validates :description, presence: true
  validates :user_id, presence: true
  validates :href, presence: true, uniqueness: { case_sensitive: false }
end
