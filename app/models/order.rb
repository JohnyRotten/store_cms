class Order < ActiveRecord::Base
  belongs_to :user
  has_many :order_items, dependent: :destroy

  validates :user_id, presence: true

  def items_count
    count = 0
    self.order_items.each do |item|
      count += item.count
    end
    count
  end

  def cost
    _r = 0
    self.order_items.each do |item|
      _r += item.price * item.count
    end
    _r
  end
end
