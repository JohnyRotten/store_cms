class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :good

  validates :order_id, presence: true
  validates :good_id, presence: true
  validates :count, presence: true
end
