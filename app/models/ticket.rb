class Ticket < ActiveRecord::Base
	belongs_to :user

	has_many :comments, as: :commentable

	enum status: [ :opened, :reviewed, :worked, :closed ]

	validates :title, presence: true, length: { in: 5..75 }
	validates :user_id, presence: true
	validates :body, presence: true
end
