class Category < ActiveRecord::Base
  belongs_to :category
  has_many :categories, dependent: :destroy
  has_many :goods
  has_and_belongs_to_many :properties

  validates :name, presence: true, length: { in: 5..20 }, uniqueness: { case_sensitive: false }
  validates :description, presence: true
  validates :category_id, presence: true

  before_save :preparing
  before_destroy :change_category

  def all_goods
    arr = []
    arr += self.goods
    categories.each do |cat|
      arr += cat.all_goods
    end
    arr
  end

  def change_category
    self.goods.each do |good|
      good.update_attribute :category_id, self.category_id
    end
  end

  def property?(property)
    self.properties.include? property
  end

  def parent_property?(property)
    return false if category.nil?
    category.property?(property) || category.parent_property?(property)
  end

  def all_properties
    arr = []
    arr += category.all_properties unless category.nil?
    properties.each { |p| arr << p }
    arr
  end

  private

   def preparing
    self.category_id ||= 0
    self.enabled ||= false
    nil
   end
end
