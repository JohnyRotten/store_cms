class Question < ActiveRecord::Base
	validates :question, presence: true, length: { minimum: 10 }

	before_create { self.enabled ||= false }
	before_create { self.answer ||= '' }
end
